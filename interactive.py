"""
2013-11-05 Eugene Yurtsev
Testing feasibility of using python to control the robotic arm.
This seems to be indeed possible.
"""
import pyevo
import sys, traceback

print "Starting interactive pyevo..."

evo = pyevo.EVO()
evo.logon()

#evo.system.Initialize()
s = pyevo.EVOScripter(evo, True)
try:
	get = """GetDITI2(255,"DiTi 200ul SBS LiHa",0,0,10,70);"""

	drop = """DropDITI(255,1,6,10,70,0);"""
	#evo.system.ExecuteScriptCommand(get)

	s.dropTips(8)
	#s.GetDiTis(8)

	# for i in range(10):
	# 	evo.system.ExecuteScriptCommand(get)
	# 	evo.system.ExecuteScriptCommand(drop)
except Exception as e:
	exc_type, exc_value, exc_traceback = sys.exc_info()
	print "*** print_tb:"
	traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
	print e
	print 'excepted'


        print "*** print_exc:"
        traceback.print_exc()
        print "*** format_exc, first and last line:"
        formatted_lines = traceback.format_exc().splitlines()
        print formatted_lines[0]
        print formatted_lines[-1]
        print "*** format_exception:"
        print repr(traceback.format_exception(exc_type, exc_value,
                                              exc_traceback))
        print "*** extract_tb:"
        print repr(traceback.extract_tb(exc_traceback))
        print "*** format_tb:"
        print repr(traceback.format_tb(exc_traceback))
        print "*** tb_lineno:", exc_traceback.tb_lineno

	evo.logoff()
#evo.shutdown()

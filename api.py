import numpy

class Robot:
    """
    
    """
    Objects = {}
    _tipcycle = {} # keys: tip types, each key has a list value, list is the order in which boxes are cycled
    _ditiloc = {} # keys: tip types, each key has a value indicating the current box in the corresponding list
    
    # Attributes
    #verbosity = 'mid'
    _workspace = None
    _mode = 'validate'
    _output = None
    _tips = numpy.ones(8)
    _tipsloaded = False
    _tipvols = numpy.zeros(8) # Volume of liquid currently in tips.
    _loadedtiptype = None
    _ccount = 0 # keeps a count of the number of commands that have been executed
    _liquidClass = 'Water free dispense'
    _username = "Admin"
    _password = "admin"
    _isRealRobot = False
    
    # Functions
    def __init__(self):
        self.incubator = Incubator('Incubator', self)
        self.platereader = Platereader('Sunrise', self)
        self.hotel = Hotel('Hotel', self)
        self.mp3pos = MP3Pos('MP3Pos', self)
        self.Objects['Incubator'] = self.incubator
        self.Objects['Platereader'] = self.platereader
        self.Objects['Hotel'] = self.hotel
        self.Objects['MP3Pos'] = self.mp3pos
        self._ccount = 0
        #self._output = open('default.esc', 'w')
        return
    
    def loadWorkspace(self,filename):
        """
        Parameters:
        filename: string, name of .esc file to laod hardware model
        
        Returns:
        None
        
        Open the workspace file to read object placements
        and populate the Objects dictionary
        """
        # read file
        self._workspace = filename
        f = open(self._workspace)
        lines = f.readlines()
        f.flush()
        f.close()
        # read meta. As of now, nothing is done with it
        workspaceID = lines[0].strip()
        workspaceModifyDate = lines[1].split()[0].split('_')[0]
        workspaceModifyTime = lines[1].split()[0].split('_')[1]
        WorkspaceCreateName = lines[1].split()[1]
        # generate workspace model
        # change this if workspace file format changes
        grid, line, mp3i = 0, 0, 0
        while grid < 30:
            if lines[line].split(';')[0] == '998':
                types = lines[line].split(';')
                nObj = int(types[1])
                if nObj > 0:
                    names = lines[line + 1].split(';')
                    site = 1
                    for i in range(nObj):
                        if types[2 + i] != '':
                            # cycle through all labware types to identify labware
                            # implement dictionary lookups for labware types
                            if types[2+i].lower().startswith('diti') == True:
                                # tip racks: add to appropriate rack cycle
                                if not types[2+i] in self._tipcycle.keys():
                                    self._tipcycle[types[2+i]] = []
                                    self._ditiloc[types[2+i]] = 0
                                self.Objects[names[1+i]] = TipBox(names[1+i],
                                                                  types[2+i], grid, site)
                                self._tipcycle[types[2+i]].append(self.Objects[names[1+i]])
                                self.Objects['MP3Pos'+format(5+6*(mp3i/3),'02d')+format(1+mp3i%3,'02d')].occupied = self.Objects[names[1+i]]
                                self.mp3pos.slots[mp3i] = self.Objects[names[1+i]]
                                self.mp3pos.slots[mp3i].occupied = 1
                                mp3i = mp3i + 1
                            elif types[2+i].lower().find('trough') != -1:
                                self.Objects[names[1+i]] = Trough(names[1+i],
                                                                  types[2+i], grid, site)
                            elif types[2+i].lower().find('plate') != -1:
                                self.Objects[names[1+i]] = PlateHolder(names[1+i],
                                                                 types[2+i], grid, site, 'MP 3Pos')
                                del self.Objects['MP3Pos'+format(5+6*(mp3i/3),'02d')+format(1+mp3i%3,'02d')]
                                self.mp3pos.slots[mp3i] = self.Objects[names[1+i]]
                                mp3i = mp3i + 1
                            elif types[2+i].lower().find('tube') != -1:
                                self.Objects[names[1+i]] = PlateHolder(names[1+i],
                                                                 types[2+i], grid, site)
                                del self.Objects['MP3Pos'+format(5+6*(mp3i/3),'02d')+format(1+mp3i%3,'02d')]
                                self.mp3pos.slots[mp3i] = self.Objects[names[1+i]]
                                self.mp3pos.slots[mp3i].occupied = 1
                                mp3i = mp3i + 1
                            elif (types[2+i].lower().startswith('washstation') == True):
                                if (types[2+i].lower().find('diti') != -1):
                                    self.Objects['Waste'] = Labware('Waste',types[2+i],
                                                                    grid, site)
                            else:
                                self.Objects[names[1+i]] = Labware(names[1+i],
                                                                   types[2+i], grid, site)
                                if nObj == 3:
                                    del self.Objects['MP3Pos'+format(5+6*(mp3i/3),'02d')+format(1+mp3i%3,'02d')]
                                    self.mp3pos.slots[mp3i] = self.Objects[names[1+i]]
                                    self.mp3pos.slots[mp3i].occupied = 1
                                    mp3i = mp3i + 1
                        else:
                            if nObj == 3:
                                mp3i = mp3i + 1
                        site = site + 1
                    line = line + 1
                grid = grid + 1
            line = line + 1
        return
    
    def listObjects(self, category='all'):
        """
        Parameters:
        category: string (='all', 'plates', 'tipboxes', 'troughs', 'devices') default='all'
        
        Returns:
        None
        
        Prints a list of object names, categories, positions present on the robot
        """
        for key in sorted(self.Objects.keys()):
            if (self.Objects[key].category == category) | (category == 'all'):
                ops = str(self.Objects[key].name).ljust(15)+':'+str(self.Objects[key].types)
                ops = ops+' at '+str(self.Objects[key].grid)+', '+str(self.Objects[key].site)
                print ops
        return
    
    def setMode(self, mode='offline', pointer=None):
        """
        Parameters:
        mode: string (='validate', 'offline', 'online')
        pointer: string, filename to write to
              or COM port(not yet defined)

        Returns:
        None
        
        When offline, commands are exported to a .esc file
        in the same directory as the script
        When set to online, tries to connect to the robot over the COM port and
        then issues following commands directly to the robot
        """
        # close previous pointers
        if self._mode == 'offline':
            self._output.flush()
            self._output.close()
        elif self._mode == 'online':
            self._output.shutdown()
            
        # open new pointers
        self._mode = mode
        if self._mode == 'offline':
            if pointer == None:
                self._output = open('default.esc', 'w')
            else:
                self._output = open(pointer, 'w')
            f = open(self._workspace)
            lines = f.readlines()
            f.flush()
            f.close()
            for line in lines:
                self._output.writelines(line)
                if line[0:11] == '--{ RPG }--':
                    break
            self._output.flush()
            return
        elif self.mode == 'online':
            import pyevo.py
            self._output = pyevo.EVO(self)
            self._output.logon()
            self._output.system.Initialize()
            print "Initialized"
            self._output.scriptID = self._output.system.PrepareScript(self._workspace)
            self._output.system.Initialize()
            self._output.system.StartScript(evo.scriptID)
            
        return
            
    def status(self, category):
        """
        Parameters:
        category: string (='all', 'tipboxes')
        
        Returns:
        None
        
        Prints status information for given type of object
        """
        if (category == 'tipboxes') | (category == 'all'):
            for key in self._tipcycle.keys():
                order = ''
                for i in range(len(self._tipcycle[key])):
                    order = order + ' ' + self._tipcycle[key][i].name
                ops = key+': Using box '+self._tipcycle[key]\
                    [self._ditiloc[key]].name+', from position '
                ops = ops+str(self._tipcycle[key][self._ditiloc[key]].nTipsUsed+1)
                print ops
                print 'Order of boxes is' + order
        if (category == 'tipboxes') | (category == 'liha'):
            print 'LiHa tip state: ' + numpy.array_str(self._tips)
            if self._tipsloaded == True:
                print 'LiHa tips loaded'
            else:
                print 'LiHa tips not loaded'
            print 'Liha tip type: ' + str(self._loadedtiptype)
        
        if (category == 'freespots') | (category == 'plateholders'):
            for key in sorted(self.Objects.keys()):
                if self.Objects[key].category == 'plateholders':
                    print self.Objects[key].name, self.Objects[key].occupied
        
        if category.lower() == 'mp3pos':
            category = 'MP 3Pos'
        if category.lower() == 'hotel':
            category = '16 Position Hotel 30034177'
        if category.lower() == 'incubator':
            category = 'Incubator1'
        for key in sorted(self.Objects.keys()):
            if self.Objects[key].category == 'plateholders':
                if category == self.Objects[key].carrierName.strip():
                    print self.Objects[key].name, self.Objects[key].occupied,\
                        self.Objects[key].grid, self.Objects[key].site
        return

    def addLabware(self, labware):
        """
        labware: Labware object
        """
        if(self._mode == 'online'):
            print 'Cannot add labware in online mode, modify worktable and restart'
        for obj in self.Objects.keys():
            if (self.Objects[obj].grid == labware.grid) & (self.Objects[obj].site == labware.site):
                if (isinstance(labware,TipBox)):
                    self.Objects[obj].occupied = labware
                else:
                    del self.Objects[obj]
                break
        self.Objects[labware.name] = labware

    def setDitiBoxOrder(self, order, types=None):
        """
        Parameters:
        order: list, list of TipBox objects in cycling order
        types: string, type of tipbox (='DiTi 200ul SBS LiHa', etc)
            or TipBox object, types set to types of TipBox object
            or None (default), uses the type of first tipbox in tipcycle dictionary
        
        Returns:
        None
        
        Sets order of tipboxes of given type to use
        """
        # normalize parameters
        types = self._normalize('tips', types)
        
        if types in self._tipcycle.keys():
            uidBox = False
            for labware in order:
                if self.Objects[labware.name].types != types:
                    uidBox = True
            if uidBox == False:
                self._tipcycle[types] = order
            else:
                print 'Unidentified tipbox in proposed order!'
        else:
            print "No tipbox of type '" + types + "' found on robot"
        return
    
    def getDiti(self, tips=None, position=None, col=None, types=None):
        """
        Parameters:
        tips: list, which tips to use (=[1,2,5,6], 1..8)
           or binary array (=array([1,1,0,0,1,1,0,0]))
           or string (='all')
           or None (default), uses previous configuration
        position: TipBox object, uses position of the tipbox
               or None (default), uses next available tips of the type specified
        col: int, column to pick from (1..8)
          or None (default), picks from the first available spot
        types: string, type of tipbox (='DiTi 200ul SBS LiHa', etc)
            or TipBox object, types set to types of TipBox object
            or None (default), uses the type of first tipbox in tipcycle dictionary
        
        Returns:
        None
        
        Picks up (specified) tips (from specified location)
        """
        # normalize
        tiptype = self._normalize('tips', types)
        tips = self._normalize('mask', tips)
        self._tips = tips
        self._loadedtiptype = tiptype
        loc = self._ditiloc[tiptype]
        
        # update position
        if position == None:
            position = self._tipcycle[tiptype][loc]
        if col != None:
            self._tipcycle[tiptype][loc].nTipsUsed = (col-1)*8
        if 96-self._tipcycle[tiptype][loc].nTipsUsed<max(numpy.where(tips==1)\
            [0])-min(numpy.where(tips==1)[0]): # not enough tips in current box
            loc = (loc + 1)%len(self._tipcycle[tiptype])
            self._ditiloc[tiptype] = loc
            position = self._tipcycle[tiptype][loc]
        coords = (self._tipcycle[tiptype][loc].grid,self._tipcycle[tiptype]\
            [loc].site-1,self._tipcycle[tiptype][loc].nTipsUsed+1)
        
        # output
        self._ccount = self._ccount + 1
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[-i-1] = tips[-i-1]*2**i
        if self._mode == 'offline':
            wstr = 'PickUp_DITIs2('+str(int(sum(tipval)))+','+str(coords[0])+','\
                +str(coords[1])+',"'
            wstr = wstr+wellSelGen(coords[2]-min(numpy.where(tips==1)[0]),tips,\
                self._tipcycle[tiptype][loc])+'",0,"'+tiptype+'",0);\n'
            self._output.write(wstr)
            self._output.flush()
        
        # update status
        self._tipcycle[tiptype][loc].nTipsUsed=self._tipcycle[tiptype][loc]\
            .nTipsUsed+max(numpy.where(tips==1)[0])-min(numpy.where(tips==1)[0])+1
        if self._tipcycle[tiptype][loc].nTipsUsed >= 96:
            self._tipcycle[tiptype][loc].nTipsUsed = 0
            self._ditiloc[tiptype] = (self._ditiloc[tiptype]+1)%len(self._tipcycle[tiptype])
        #else
        self._tipsloaded = True
        return
    
    def setDiti(self, position=None):
        """
        Parameters:
        position: TipBox object, uses position of the tipbox
               or None (default), uses next available tips of the type specified
        
        Returns:
        None
        
        Sets back mounted tips (to specified location)
        """
        # normalize
        tiptype = self._loadedtiptype
        tips = self._tips
        loc = self._ditiloc[tiptype]
        self._tips = tips
        
        # select position
        nReplaced = max(numpy.where(tips==1)[0]) - min(numpy.where(tips==1)[0]) + 1
        if position == None:
            if self._tipcycle[tiptype][loc].nTipsUsed >= nReplaced: # loaded tips can fit in current box
                self._tipcycle[tiptype][loc].nTipsUsed = self._tipcycle[tiptype][loc].nTipsUsed-nReplaced
            else: # go to previous box
                self._ditiloc[tiptype] = (self._ditiloc[tiptype]-1)%len(self._tipcycle[tiptype])
                loc = self._ditiloc[tiptype]
                self._tipcycle[tiptype][loc].nTipsUsed = 96 - nReplaced
            coords = (self._tipcycle[tiptype][loc].grid, \
                self._tipcycle[tiptype][loc].site, self._tipcycle[tiptype][loc].nTipsUsed+1)
        if isinstance(position,Labware):
            coords = (position.grid, position.site-1, position.nTipsUsed+1)
        # output
        self._ccount = self._ccount + 1
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[-i-1] = tips[-i-1]*2**i
        if self._mode == 'offline':
            wstr = 'Set_DITIs_Back('+str(int(sum(tipval)))+','+str(coords[0])+','+str(coords[1])
            wstr = wstr+',"'+wellSelGen(coords[2],tips,self._tipcycle[tiptype][loc])+'",0,0);\n'
            self._output.write(wstr)
            self._output.flush()
        #else
        self._tipsloaded = False
        return
    
    def mix(self, plate, col, volume, cycles=3, wells=None, tips=None, liquidClass=None, offset=0):
        """
        Parameters:
        plate: Plate object
            or PlateHolder object
            or string, name of labware
        col: int, 1..8
        volume: int, in ul, used for all wells
             or list, in ul, only for wells being used
        cycles: int
        wells: list, which wells to mix (=[1,2,4], 1..8)
            or binary array (=array([1,1,0,1]))
            or None (default), mixes with all loaded tips, can still have an offset
        offset: int, shift the wells pattern up or down by some amount
        liquidClass: string
        
        Returns:
        None
        
        Issue the Mix command
        """
        # normalize
        liquidClass = self._liquidClass
        wells = self._normalize('mask', wells)
        volume = self._normalize('volume', volume)
        plate = self._normalize('plate', plate)
        tips = self._normalize('mask', tips)
        
        # ensure tips are loaded
        if self._tipsloaded == False: #TODO check exact loaded pattern
            print 'No tips loaded'
            return
        
        tips = _genTipOffsetPattern(tips, wells)
        if isinstance(tips, int):
            print 'Some of the required tips not loaded'
            return
        
        # output
        self._ccount = self._ccount + 1
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self._mode == 'offline':
            wstr = 'Mix('+str(int(sum(tipval)))+',"'+liquidClass+'",'
            for i in range(8):
                if tips[i] == 0:
                    wstr = wstr + '0,'
                else:
                    wstr = wstr + '"' + str(int(volume[i])) +'",'
            wstr = wstr + '0,0,0,0,'
            wstr = wstr+str(plate.holder.grid)+','+str(plate.holder.site-1)+','+str(1)+','+'"'
            wstr = wstr+wellSelGen((col-1)*8+1+offset,wells,plate.holder)+'",'+str(cycles)+',0,0);\n'
            self._output.write(wstr)
            self._output.flush()
        return
    
    def aspirate(self, plate, col, volume, wells=None, tips=None, liquidClass=None, offset=0):
        """
        Parameters:
        plate: Plate object
            or PlateHolder object
            or string, name of labware
        col: int, 1..8
        volume: int, in ul, used for all wells
             or list, in ul, only for wells being used
        wells: list, which wells to mix (=[1,2,4], 1..8)
            or binary array (=array([1,1,0,1]))
            or None (default), mixes with all loaded tips, can still have an offset
        offset: int, shift the wells pattern up or down by some amount
        liquidClass: string
        
        Returns:
        None
        
        Issue the Aspirate command
        """
        # normalize
        liquidClass = self._liquidClass
        wells = self._normalize('mask', wells)
        volume = self._normalize('volume', volume)
        plate = self._normalize('plate', plate)
        tips = self._normalize('mask', tips)

        # ensure tips are loaded
        if self._tipsloaded == False: #TODO check exact loaded pattern
            print 'No tips loaded'
            return
        
        tips = _genTipOffsetPattern(tips, wells)
        if isinstance(tips, int):
            print 'Some of the required tips not loaded'
            return
        
        # ensure tips are not overfilled.
        maxCap = _ditiloc[self._loadedtiptype].maxcapacity
        if numpy.any(self.tipvols + volume > maxCap * 0.8):
            print 'Overfilled tips'
            return

        # output
        self._tipvols = self._tipvols + volume #update current volume in tips.
        self._ccount = self._ccount + 1
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self._mode == 'offline':
            wstr = 'Aspirate('+str(int(sum(tipval)))+',"'+liquidClass+'",'
            for i in range(8):
                if tips[i] == 0:
                    wstr = wstr + '0,'
                else:
                    wstr = wstr + '"' + str(int(volume[i])) +'",'
            wstr = wstr + '0,0,0,0,'
            wstr = wstr+str(plate.holder.grid)+','+str(plate.holder.site-1)+','+str(1)+','+'"'+\
                wellSelGen((col-1)*8+1+offset,wells,plate.holder)+'",0,0);\n'
            self._output.write(wstr)
            self._output.flush()
        return
    
    def dispense(self, plate, col, volume, wells=None, tips=None, liquidClass=None, offset=0):
        """
        Parameters:
        plate: Plate object
            or PlateHolder object
            or string, name of plateHolder
        col: int, 1..8
        volume: int, in ul, used for all wells
             or list, in ul, only for wells being used
        wells: list, which wells to mix (=[1,2,4], 1..8)
            or binary array (=array([1,1,0,1]))
            or None (default), mixes with all loaded tips, can still have an offset
        offset: int, shift the wells pattern up or down by some amount
        liquidClass: string
        
        Returns:
        None
        
        Issue the Dispense command
        """
        # normalize
        liquidClass = self._liquidClass
        wells = self._normalize('mask', wells)
        volume = self._normalize('volume', volume)
        plate = self._normalize('plate', plate)
        tips = self._normalize('mask', tips)
        
        # ensure tips are loaded
        if self._tipsloaded == False: #TODO check exact loaded pattern
            print 'No tips loaded'
            return
        
        tips = _genTipOffsetPattern(tips, wells)
        if isinstance(tips, int):
            print 'Some of the required tips not loaded'
            return
        
        # ensure there is enough liquid
        if numpy.any(self._tipvols < volume):
            print 'Not enough liquid in the tips!'
            print 'Requested: ' + str(volume)
            print 'Actually contains: ' + str(self._tipvols)
            return

        # output
        self._ccount = self._ccount + 1
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self._mode == 'offline':
            wstr = 'Dispense('+str(int(sum(tipval)))+',"'+liquidClass+'",'
            for i in range(8):
                if tips[i] == 0:
                    wstr = wstr + '0,'
                else:
                    wstr = wstr + '"' + str(int(volume[i])) +'",'
            wstr = wstr + '0,0,0,0,'
            wstr = wstr+str(plate.holder.grid)+','+str(plate.holder.site-1)+','+str(1)+','+'"'+\
                wellSelGen((col-1)*8+1+offset,wells,plate.holder)+'",0,0);\n'
            self._output.write(wstr)
            self._output.flush()
        return
    
    def dropDiti(self, tips=None):
        """
        Parameters:
        tips: list, which tips to use (=[1,2,5,6], 1..8)
           or binary array (=array([1,1,0,0,1,1,0,0]))
           or string (='all')
           or None (default), uses previous configuration
        
        Returns:
        None
        
        Drops tips in waste
        """
        if self._tipsloaded == False:
            print 'No tips loaded'
            return
        tips = self._normalize('mask', tips)
        
        # output
        self._ccount = self._ccount + 1
        tipval = tips.copy()
        for i in range(len(tips)):
            tipval[i] = tips[i]*2**i
        if self._mode == 'offline':
            wstr = 'DropDITI('+str(int(sum(tipval)))+','+str(self.Objects['Waste'].grid)
            wstr = wstr+','+str(self.Objects['Waste'].site-1)+',10,70,0);\n'
            self._output.write(wstr)
            self._output.flush()
        self._tipsloaded = False
        return
    
    def transferLabware(self, sourceplate, destination, lid=None, uncover=True):
        """
        Parameters:
        source: Plate object
             or PlateHolder object
             or string, name of the labware
        destination: PlateHolder object
                  or string, name of the labware
        lid: PlateHolder object
          or string, name of the labware
          or None, if you do not want lid handling
        uncover: boolean, if true, uncover at destination
                          if false, cover at source
        
        Returns:
        None
        
        Move around plates
        """
        # Validate
        # select plates
        lidhandling = True
        source = self._normalize('plateholder',sourceplate)
        destination = self._normalize('plateholder',destination)
        if lid == None:
            lidhandling = False
            lid = PlateHolder('tmp', source.types, 0, 0, '')
        lid = self._normalize('plateholder',lid)
        
        # output
        self._ccount = self._ccount + 1
        if self._mode == 'offline':
            wstr = 'Transfer_Rack("'+str(int(source.grid))+'","'+str(int(destination.grid))+'",0,'+str(int(lidhandling))+',0,0,'+str(int(uncover))+',"'
            wstr = wstr +str(int(lid.grid))
            wstr += '","'+source.types+'","'+'Narrow'+'","","","'+source.carrierName+'","'+lid.carrierName+'","'+destination.carrierName+'","'
            wstr = wstr +str(int(source.site))+'","'+str(int(lid.site))+'","'+str(int(destination.site))+'");\n'
            self._output.write(wstr)
            self._output.flush()
        
        # update status
        source.occupied = None
        if uncover == False:
            lid.occupied = None
        if isinstance(sourceplate, Plate):
            destination.occupied = sourceplate
            sourceplate.holder = destination    # interwined, curious!
        else:
            destination.occupied = 1
        return
    
    def __del__(self):
        """
        Clean up, close file pointers or log off
        """
        self._output.flush()
        self._output.close()

    def _normalize(self, what, param, **kwargs):
        """
        This overloaded function is used to convert different input
        forms to a standardized form for various cases
        """
        retval = param
        
        if what == 'tips':
            if isinstance(param, str):
                if param in self._tipcycle.keys():
                    retval = param
                else:
                    print 'Tip type not available'
                    retval = -1
            elif isinstance(param, TipBox):
                retval = param.types
            elif param == None: # TODO: add exception if there is no tip box
                retval = self._tipcycle.keys()[0]
        
        elif what == 'mask':
            if param == None:
                retval = self._tips
            elif param == 'all':
                retval = numpy.ones(8)
            elif isinstance(param,list):
                retval = numpy.zeros(8)
                for i in param:
                    retval[i-1] = 1
        
        elif what == 'volume':
            if (isinstance(param,float)) | (isinstance(param,int)):
                retval = []
                for i in range(8):
                    retval.append(param)
            elif isinstance(param, list):
                retval = param.copy()
        
        elif what == 'plateholder':
            if not isinstance(param, PlateHolder):
                if isinstance(param, Plate):
                    retval = param.holder
                else:
                    retval = self.Objects[param]
        
        elif what == 'plate':
            if not isinstance(param, Plate):
                if isinstance(param, str):
                    retval = self.Objects[param].occupied
                elif isinstance(param.occupied, Plate):
                    retval = param.occupied
                else:
                    retval = Plate(holder=param)
                    retval.covered = False
        return retval
    
    # These are not being used as of now, but would be nice to have
    # a single output generating function rather than output statements
    # in each command definition
    def _generateOutput(command, **a):
        if command == 'PickUp_DITIs2':
            if self._mode == 'offline':
                wstr = command+'('+_commas(a['tipMask'],a['grid'],a['site'],\
                    qu(a['wellSelection']),a['noOfLoopOptions'])
                loop = 0
                while loop < a[noOfLoopOptions]:
                    wstr = wstr + ''
                    loop = loop + 1
                wstr = wstr+");\n"
                self._output.write(wstr)
                self._output.flush()
                return
    
    def _qu(string, force=False):
        if (force == False) & (str(string) == '0'):
            return str(string)
        return '"' + str(string) + '"'
    
    def _commas(*args):
        rstr = ''
        for string in args:
            rstr = rstr + ',' + str(string)
        return rstr[1:]

class Labware:
    """
    
    """
    name = None
    types = None
    grid = None
    site = None
    category = None
    xlen = 12
    ylen = 8
    
    def __init__(self, name, types, grid, site):
        self.name = name
        self.grid = grid
        self.site = site
        self.types = types
        self.xlen = lwdict[types][0]
        self.ylen = lwdict[types][1]

class TipBox(Labware):
    """
    
    """
    category = 'tipboxes'
    nTipsUsed = 0
    maxcapacity = 200 #Max listed capacity of each tip in uL. Tecan enforces something like 80% of this.
    # list all types below
    DiTi_200ul_SBS_LiHa = 'DiTi 200ul SBS LiHa'

    def __init__(self, name, types, grid, site, maxcapacity):
        Labware.__init__(self, name, types, grid, site)
        self.maxcapacity = maxcapacity

class Trough(Labware):
    """
    
    """
    category = 'troughs'

class PlateHolder(Labware):
    """
    
    """
    category = 'plateholders'
    occupied = None # can be a Plate object if it is sitting there, or 1 if it is an undefined Plate
    carrierName = None
    
    def __init__(self, name, types, grid, site, carrier):
        Labware.__init__(self, name, types, grid, site)
        self.carrierName = carrier

class Device:
    """
    
    """
    name = None
    types = None
    grid = None
    site = None
    parent = None
    category = 'devices'
    carrier = None
    
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent

class Incubator(Device):
    """
    
    """
    types = 'Incubator1'
    carrier = True
    nSlots = 6
    slots = []
    grid = 26
    
    def __init__(self, name, parent):
        Device.__init__(self, name, parent)
        for i in range(self.nSlots):
            name = 'Incubator'+str(i)
            ph = PlateHolder(name, 'Default Plate', self.grid, i, self.types)
            self.slots.append(ph)
            self.parent.Objects[name] = ph
    
    def closeDoors(self):
        self.parent._ccount = self.parent._ccount + 1
        if self.parent._mode == 'offline':
            self.parent._output.write(
                'FACTS("Incubator1","Incubator1_CloseDoor","","0","");\n')
            self.parent._output.flush()
        return
    
    def setTemperature(self, temperature, slots):
        """
        Parameters:
        temperature: int, in celcius (float is probably fine)
        Requires the slots argument to be a list, not an integer
        Say setTemperature(30, [2]), not setTemperature(30, 2)
        """
        self.parent._ccount = self.parent._ccount + 1
        if (slots == 'all') | (set(slots) == set([1,2,3,4,5,6])):
            slots = numpy.array([0])
        if self.parent._mode == 'offline':
            for slot in slots:
                wstr = 'FACTS("Incubator1","Incubator1_SetTemperature","'
                wstr += str(slot)+','+str(temperature)+'","0","");\n'
                self.parent._output.write(wstr)
                self.parent._output.flush()
        #else:
            
        return
    
    def _get_loc():
        """
        Retrns a PlateHolder object to move things to/from
        
        """
        return

class Platereader(Device):
    """
    
    """
    types = 'Sunrise'
    carrier = True
    nSlots = 1
    slots = []
    grid = 3
    
    def __init__(self, name, parent):
        Device.__init__(self, name, parent)
        name = 'Sunrise0'
        ph = PlateHolder(name, 'Default Plate', self.grid, 1, self.types)
        self.slots.append(ph)
        self.parent.Objects[name] = ph
    
    def close(self):
        self.parent._ccount = self.parent._ccount + 1
        if self.parent._mode == 'offline':
            self.parent._output.write("FACTS(\"Sunrise\",\
                \"Sunrise_Close\",\"\",\"0\",\"\");\n")
            self.parent._output.flush()
        return
    
    def open(self):
        self.parent._ccount = self.parent._ccount + 1
        if self.parent._mode == 'offline':
            self.parent._output.write("FACTS(\"Sunrise\",\
                \"Sunrise_Open\",\"\",\"0\",\"\");\n")
            self.parent._output.flush()
        return
    
    def measure(self):
        self.parent._ccount = self.parent._ccount + 1
        return
    
    def _get_loc():
        return

class Hotel(Device):
    """
    
    """
    types = '16 Position Hotel 30034177'
    carrier = True
    nSlots = 16
    slots = []
    grid = 4
    
    def __init__(self, name, parent):
        Device.__init__(self, name, parent)
        for i in range(self.nSlots):
            name = 'Hotel'+format(i,'02d')
            ph = PlateHolder(name, 'Default Plate', self.grid,\
                i, self.types)
            self.slots.append(ph)
            self.parent.Objects[name] = ph
    
    def _get_loc():
        return

class MP3Pos(Device):
    """
    
    """
    types = 'MP 3Pos'
    carrier = True
    nSlots = 12
    slots = []
    
    def __init__(self, name, parent):
        Device.__init__(self, name, parent)
        for i in range(self.nSlots):
            name = 'MP3Pos'+format(5+6*(i/3),'02d')+format(1+i%3,'02d')
            ph = PlateHolder(name, 'Default Plate', 5+6*(i/3), 1+i%3, self.types)
            self.slots.append(ph)
            self.parent.Objects[name] = ph
    
    def _get_loc():
        return

class Plate:
    """
    
    """
    types = None
    xlen = 12
    ylen = 8
    wells = []
    holder = None
    lidHolder = None
    covered = True
    
    def __init__(self, holder=None, types=None):
        """
        Parameters:
        holder: PlateHolder object, where the plate starts
        types: string, type of labware, default plateholder value
        """
        self.holder = holder
        self.types = holder.types
        if not types == None:
            self.types = types
            self.holder.types = types
        self.holder.occupied = self
        for i in range(self.xlen):
            wellx = []
            for j in range(self.ylen):
                wellx.append(Well(i,j,self))
            self.wells.append(wellx)
        self.xlen = lwdict[self.types][0]
        self.ylen = lwdict[self.types][1]
    
    P96_Well_Microplate_F3072 = '96 Well Microplate F3072'

class Well:
    """
    
    """
    filledVolume = 0
    xpos = 0
    ypos = 0
    plate = None
    
    def __init__(self, xpos, ypos, plate):
        self.xpos = xpos
        self.ypos = ypos
        self.plate = plate

def wellSelGen(start, tips, labware):
    """
    start: index where sequence tips starts
    tips: binary sequence
    """
    #print start, tips
    retstr = ''
    start = start - 1
    xlen = labware.xlen
    ylen = labware.ylen
    retstr = retstr + format(xlen,'02x').upper() + format(ylen,'02x').upper()
    nWells = xlen*ylen
    wells = numpy.zeros(nWells)
    wells[start:start+len(tips)] = tips
    #for i in range(8):
        #for j in range(12):
            #print int(wells[i+8*j]),
        #print '\n'
    igroup = 0
    while igroup < nWells:
        seq = numpy.zeros(7)
        seq[0:min(7,nWells-igroup)] = wells[igroup:igroup+min(7,nWells-igroup)]
        for i in range(len(seq)):
            seq[i] = seq[i]*2**i
        seqval = int(sum(seq))+48
        retstr = retstr + chr(seqval)
        igroup = igroup + 7
    return retstr

def _genTipOffsetPattern(tips, wells):
    """
    Finds the pattern in wells within tips, and returns a new
    array of tips which is the wells array without the offset
    """
    # ensure well pattern is a subset of loaded tip pattern
    match = False
    minimalwell = wells[min(numpy.where(wells==1)[0]):max(numpy.where(wells==1)[0])+1]
    #print wells
    #print minimalwell
    if len(minimalwell) > 8:
        return -1
    for offset in range(8-len(minimalwell)):
        match = True
        for i in range(len(minimalwell)):
            if (minimalwell[i] == 1) & (tips[i+offset] == 0):
                match = False
                break
        if match == True:
            break
    if match == False:
        return -1
    tips = numpy.zeros(8)
    #print offset, len(minimalwell)
    for i in range(len(minimalwell)):
        tips[i+offset] = minimalwell[i]
    #print tips
    return tips

lwdict = {
    'Trough 100ml'                :(1,1),
    'Washstation 2Grid DiTi Waste':(1,1),
    'Washstation 2Grid Waste'     :(1,1),
    'DiTi 200ul SBS LiHa'         :(12,8),
    '96 Well Microplate F3072'    :(12,8),
    'Default Plate'               :(12,8)
}